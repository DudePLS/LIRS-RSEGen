#!/usr/bin/env python
# author: Ruslan Gabdrahmanov
import math
import os
from datetime import datetime

import numpy

from strings import mtl, config, sdf, vertex_str, import_xml_str


class Generator:
    def __init__(self, size, divided, chunk_count, modeling="single model", height_step=0, obstacles=None,
                 first_half=True):
        if obstacles is None:
            obstacles = []
        self.size = size
        if divided == "no (one piece)":
            self.division = 0
        elif divided == "yes (divided)":
            self.division = 2
        else:
            self.division = 1
        self.chunk_count = chunk_count
        self.modeling = modeling
        self.height_step = height_step / 100
        self.scaled_height_step = self.height_step / 10
        self.obstacles = obstacles
        self.first_half = first_half
        try:
            self.base = open('surroundings', 'r')
            self.block = open('block', 'r')
            if not os.path.isdir('generated'):
                os.mkdir('generated')
            self.save_dir = 'generated'
        except FileNotFoundError:
            self.base = open('engineer_gazebo/world/utils/test-rse-gen/surroundings', 'r')
            self.block = open('engineer_gazebo/world/utils/test-rse-gen/block', 'r')
            if not os.path.isdir('engineer_gazebo/world/utils/test-rse-gen/generated'):
                os.mkdir('engineer_gazebo/world/utils/test-rse-gen/generated')
            self.save_dir = 'engineer_gazebo/world/utils/test-rse-gen/generated'
        if not os.path.isdir(self.save_dir + '/models'):
            os.mkdir(self.save_dir + '/models')
        self.base_str = self.base.read()
        self.block_str = self.block.read()
        self.full_size = (self.size + self.division) * self.chunk_count - 1 * self.division
        self.block_hw = 0.1
        self.base_vertex = [[self.block_hw, 0.000000, 0.000000],
                            [self.block_hw, 0.000000, self.height_step],
                            [0.000000, 0.000000, self.height_step],
                            [0.000000, 0.000000, 0.000000],
                            [self.block_hw, self.block_hw, 0.000000],
                            [self.block_hw, self.block_hw, self.height_step],
                            [0.000000, self.block_hw, self.height_step],
                            [0.000000, self.block_hw, 0.000000]]

    def generate(self, i):
        """
        Run generation
        """
        matrix = numpy.zeros((self.full_size, self.full_size))
        for obstacle in self.obstacles:
            gen_function = obstacle["gen_function"]
            if gen_function == "Random Gaussian":
                matrix = self.normal_matrix(matrix, obstacle)
            elif gen_function == "Random":
                matrix = self.random_matrix(matrix, obstacle)
            elif gen_function == "Diagonal":
                matrix = self.diagonal_matrix(matrix, obstacle)
            elif gen_function == "Horizontal":
                matrix = self.diagonal_matrix(matrix, obstacle, True)
            elif gen_function == "Peak":
                matrix = self.peak_matrix(matrix, obstacle)
            else:
                return None
        xml_str = ''
        #print(matrix)

        if self.modeling == "single model":
            prefix = f"""# File Created: {datetime.now()}
mtllib rse.mtl
"""
            xml_str = import_xml_str(i)
            obj_str, flat_str = "", ""
            for x in range(self.full_size):  # line
                for y in range(self.full_size):  # column
                    obj_str, flat_str = self.set_obj_block(obj_str, flat_str, matrix, x, y)
            if self.division > 0:
                obj_str, flat_str = self.add_obj_borders(obj_str, flat_str)
            name = self.save_dir + "/models/test_rse" + str(i) + "/meshes/rse.obj"
            mtl_name = self.save_dir + "/models/test_rse" + str(i) + "/meshes/rse.mtl"
            sdf_name = self.save_dir + "/models/test_rse" + str(i) + "/model.sdf"
            config_name = self.save_dir + "/models/test_rse" + str(i) + "/model.config"
            if not os.path.isdir(self.save_dir + '/models/test_rse' + str(i)):
                os.mkdir(self.save_dir + '/models/test_rse' + str(i))
            if not os.path.isdir(self.save_dir + '/models/test_rse' + str(i) + "/meshes"):
                os.mkdir(self.save_dir + '/models/test_rse' + str(i) + "/meshes")
            res = open(name, 'w')
            res.writelines(prefix + obj_str + vertex_str + flat_str)
            res = open(mtl_name, 'w')
            res.writelines(mtl)
            res = open(sdf_name, 'w')
            res.writelines(sdf(i))
            res = open(config_name, 'w')
            res.writelines(config)
        elif self.modeling == "multi model":
            if self.division > 0:
                xml_str = self.add_borders(xml_str)
            for x in range(self.full_size):  # line
                for y in range(self.full_size):  # column
                    xml_str = self.set_block(xml_str, matrix, x, y)
        name = self.save_dir + "/test_rse" + str(i) + ".sdf"
        res = open(name, 'w')
        res.writelines(self.base_str.replace('###models###', xml_str))

    def normal_matrix(self, matrix, obstacle):
        """
        Normalized random obstacle
        """
        strength = obstacle["strength"]
        limit = numpy.max(matrix)
        limit = strength * 10 if limit < strength * 10 else limit
        summ = self.size + self.division
        strength2 = (strength + 1)/2
        for line in range(self.chunk_count):
            for column in range(self.chunk_count):
                matrix[line * summ:line * summ + self.size,
                column * summ:column * summ + self.size] = numpy.clip(
                    matrix[line * summ:line * summ + self.size,
                           column * summ:column * summ + self.size] + numpy.round(numpy.random.normal(
                    strength2, strength2, (self.size, self.size))) * 10, 0, limit)
        return matrix

    def random_matrix(self, matrix, obstacle):
        """
        Random obstacle
        """
        strength = obstacle["strength"]
        limit = numpy.max(matrix)
        limit = strength * 10 if limit < strength * 10 else limit
        summ = self.size + self.division
        for line in range(self.chunk_count):
            for column in range(self.chunk_count):
                matrix[line * summ:line * summ + self.size,
                column * summ:column * summ + self.size] = numpy.clip(
                    matrix[line * summ:line * summ + self.size,
                           column * summ:column * summ + self.size] + numpy.random.random_integers(
                    0, strength, (self.size, self.size)) * 10, 0, limit)
        return matrix

    def diagonal_matrix(self, matrix, obstacle, horizontal=False):
        """
        Diagonal obstacle
        """
        left_first = obstacle["left_first"] if obstacle["left_first"] < obstacle["left_second"] else obstacle[
            "left_second"]
        left_second = obstacle["left_second"] if obstacle["left_first"] < obstacle["left_second"] else obstacle[
            "left_first"]
        if not horizontal:
            right_first = obstacle["right_first"] if obstacle["right_first"] < obstacle["right_second"] else obstacle[
                "right_second"]
            right_second = obstacle["right_second"] if obstacle["right_first"] < obstacle["right_second"] else obstacle[
                "right_first"]
        strength = obstacle["strength"]
        softness = obstacle["softness"] / 100 / self.chunk_count
        summ = self.size + self.division
        mid_height = (strength * 10)
        if not horizontal:
            diagonal = numpy.array([numpy.random.random_integers(left_first - 1, left_second - 1),
                                    numpy.random.random_integers(right_first - 1, right_second - 1)])
        else: #  horizontal obstacle is diagonal with same x or y coords
            y = numpy.random.random_integers(left_first - 1, left_second - 1)
            diagonal = numpy.array([y, y])
        diagonal = numpy.array([[diagonal[0], 0], [diagonal[1], self.full_size - 1]])
        for line in range(self.chunk_count):
            for column in range(self.chunk_count):
                for a in range(line * summ, line * summ + self.size):
                    for b in range(column * summ, column * summ + self.size):
                        dist = numpy.linalg.norm(
                            (
                                numpy.cross(
                                    diagonal[1] - diagonal[0], diagonal[0] - [a, b]
                                )
                            ) / numpy.linalg.norm(
                                diagonal[1] - diagonal[0]
                            ))
                        if softness != 0:
                            inv_height = dist / summ / self.chunk_count * mid_height / softness 
                            inv_height = inv_height - inv_height % 10
                        else:
                            inv_height = 0 if dist == 0 else mid_height
                        
                        matrix[a][b] = round(max(
                            matrix[a][b],
                            mid_height - inv_height if not (
                                    math.isnan(inv_height) or math.isinf(inv_height)) else 0
                        ), obstacle["strength"])
        return matrix

    def peak_matrix(self, matrix, obstacle):
        """
        Peak obstacle
        """
        left_first = obstacle["left_first"] if obstacle["left_first"] < obstacle["left_second"] else obstacle[
            "left_second"]
        left_second = obstacle["left_second"] if obstacle["left_first"] < obstacle["left_second"] else obstacle[
            "left_first"]
        right_first = obstacle["right_first"] if obstacle["right_first"] < obstacle["right_second"] else obstacle[
            "right_second"]
        right_second = obstacle["right_second"] if obstacle["right_first"] < obstacle["right_second"] else obstacle[
            "right_first"]
        strength = obstacle["strength"]
        softness = obstacle["softness"] / 100 / self.chunk_count
        mid_height = (strength * 10)
        summ = self.size + self.division
        x = numpy.random.random_integers(left_first - 1, left_second - 1)
        y = numpy.random.random_integers(right_first - 1, right_second - 1)
        points = numpy.array([[x, y], [x-1, y], [x, y-1], [x-1, y-1]])
        for line in range(self.chunk_count):
            for column in range(self.chunk_count):
                for a in range(line * summ, line * summ + self.size):
                    for b in range(column * summ, column * summ + self.size):
                        min_dist = self.size
                        for point in points:
                            dist = numpy.linalg.norm(point - [a, b])
                            if dist % 1 != 0:
                                if dist % 1 > 0.49:
                                    dist = dist // 1 + 1
                                else:
                                    dist = dist // 1
                            if dist < min_dist:
                                min_dist = dist
                        if softness != 0:
                            inv_height = min_dist / summ / self.chunk_count * mid_height / softness
                            inv_height = inv_height - inv_height % 10
                        else:
                            inv_height = 0 if min_dist == 0 else mid_height
                        matrix[a][b] = round(max(
                            matrix[a][b],
                            mid_height - inv_height if not (
                                    math.isnan(inv_height) or math.isinf(inv_height)) else 0
                        ), obstacle["strength"])
        return matrix

    def add_borders(self, xml_str):
        """
        Create frame around entire RSE in xml
        """
        xml_str += self.block_str.replace(
            'NAME', 'border_right').replace(
            'X', str(self.full_size / 20.0 - 0.05)).replace(
            'Y', str(-0.1)).replace(
            'S', str(self.height_step)).replace(
            'A', str(self.full_size / 10.0)).replace(
            'B', str(0.1)).replace(
            'VP', str(self.height_step / 2))
        xml_str += self.block_str.replace(
            'NAME', 'border_left').replace(
            'X', str(self.full_size / 20.0 - 0.05)).replace(
            'Y', str(self.full_size / 10)).replace(
            'S', str(self.height_step)).replace(
            'A', str(self.full_size / 10.0)).replace(
            'B', str(0.1)).replace(
            'VP', str(self.height_step / 2))
        xml_str += self.block_str.replace(
            'NAME', 'border_forward').replace(
            'X', str(-0.1)).replace(
            'Y', str(self.full_size / 20.0 - 0.05)).replace(
            'S', str(self.height_step)).replace(
            'A', str(0.1)).replace(
            'B', str(self.full_size / 10.0 + 0.2)).replace(
            'VP', str(self.height_step / 2))
        xml_str += self.block_str.replace(
            'NAME', 'border_back').replace(
            'X', str(self.full_size / 10.0)).replace(
            'Y', str(self.full_size / 20.0 - 0.05)).replace(
            'S', str(self.height_step)).replace(
            'A', str(0.1)).replace(
            'B', str(self.full_size / 10.0 + 0.2)).replace(
            'VP', str(self.height_step / 2))
        return xml_str

    def set_block(self, xml_str, matrix, x, y):
        """
        Set one block of RSE in xml
        """
        xml_str += self.block_str.replace(
            'NAME', 'block' + str(x) + str(y)).replace(
            'X', str(x / 10.0)).replace(
            'Y', str(y / 10.0)).replace(
            'S', str(matrix[x][y] * self.scaled_height_step if matrix[x][y] > 0 else self.height_step)).replace(
            'A', str(0.1)).replace(
            'B', str(0.1)).replace(
            'VP', str(matrix[x][y] * self.scaled_height_step / 2))
        return xml_str

    def set_obj_block(self, obj_str, flat_str, matrix, x, y):
        """
        Set one block of RSE in mesh
        """
        round_height = round(matrix[x][y] * self.scaled_height_step, 6) if matrix[x][y] > 0 else self.height_step / 2
        obj_str += f"""v {self.base_vertex[0][0] + x / 10} {self.base_vertex[0][1] + y / 10} {self.base_vertex[0][2]}
v {self.base_vertex[1][0] + x / 10} {self.base_vertex[1][1] + y / 10} {round_height}
v {self.base_vertex[2][0] + x / 10} {self.base_vertex[2][1] + y / 10} {round_height}
v {self.base_vertex[3][0] + x / 10} {self.base_vertex[3][1] + y / 10} {self.base_vertex[3][2]}
v {self.base_vertex[4][0] + x / 10} {self.base_vertex[4][1] + y / 10} {self.base_vertex[4][2]}
v {self.base_vertex[5][0] + x / 10} {self.base_vertex[5][1] + y / 10} {round_height}
v {self.base_vertex[6][0] + x / 10} {self.base_vertex[6][1] + y / 10} {round_height}
v {self.base_vertex[7][0] + x / 10} {self.base_vertex[7][1] + y / 10} {self.base_vertex[7][2]}
"""
        number_count = (y + x * self.full_size) * 8
        flat_str += f"""f {number_count + 1}//1 {number_count + 2}//1 {number_count + 3}//1 {number_count + 4}//1
f {number_count + 5}//2 {number_count + 8}//2 {number_count + 7}//2 {number_count + 6}//2
f {number_count + 1}//3 {number_count + 5}//3 {number_count + 6}//3 {number_count + 2}//3
f {number_count + 2}//4 {number_count + 6}//4 {number_count + 7}//4 {number_count + 3}//4
f {number_count + 3}//5 {number_count + 7}//5 {number_count + 8}//5 {number_count + 4}//5
f {number_count + 5}//6 {number_count + 1}//6 {number_count + 4}//6 {number_count + 8}//6
"""
        return obj_str, flat_str

    def add_obj_borders(self, obj_str, flat_str):
        """
        Create frame around entire RSE in mesh
        """
        obj_str += f"""v {self.block_hw + self.full_size / 10} {-self.block_hw} {0}
v {self.block_hw + self.full_size / 10} {-self.block_hw} {self.height_step}
v {0} {-self.block_hw} {self.height_step}
v {0} {-self.block_hw} {0}
v {self.block_hw + self.full_size / 10} {0} {0}
v {self.block_hw + self.full_size / 10} {0} {self.height_step}
v {0} {0} {self.height_step}
v {0} {0} {0}
"""
        number_count = (self.full_size * self.full_size) * 8
        flat_str += f"""f {number_count + 1}//1 {number_count + 2}//1 {number_count + 3}//1 {number_count + 4}//1
f {number_count + 5}//2 {number_count + 8}//2 {number_count + 7}//2 {number_count + 6}//2
f {number_count + 1}//3 {number_count + 5}//3 {number_count + 6}//3 {number_count + 2}//3
f {number_count + 2}//4 {number_count + 6}//4 {number_count + 7}//4 {number_count + 3}//4
f {number_count + 3}//5 {number_count + 7}//5 {number_count + 8}//5 {number_count + 4}//5
f {number_count + 5}//6 {number_count + 1}//6 {number_count + 4}//6 {number_count + 8}//6
"""
        obj_str += f"""v {self.block_hw + self.full_size / 10} {0} {0}
v {self.block_hw + self.full_size / 10} {0} {self.height_step}
v {self.full_size / 10} {0} {self.height_step}
v {self.full_size / 10} {0} {0}
v {self.block_hw + self.full_size / 10} {self.block_hw + self.full_size / 10} {0}
v {self.block_hw + self.full_size / 10} {self.block_hw + self.full_size / 10} {self.height_step}
v {self.full_size / 10} {self.block_hw + self.full_size / 10} {self.height_step}
v {self.full_size / 10} {self.block_hw + self.full_size / 10} {0}
"""
        number_count += 8
        flat_str += f"""f {number_count + 1}//1 {number_count + 2}//1 {number_count + 3}//1 {number_count + 4}//1
f {number_count + 5}//2 {number_count + 8}//2 {number_count + 7}//2 {number_count + 6}//2
f {number_count + 1}//3 {number_count + 5}//3 {number_count + 6}//3 {number_count + 2}//3
f {number_count + 2}//4 {number_count + 6}//4 {number_count + 7}//4 {number_count + 3}//4
f {number_count + 3}//5 {number_count + 7}//5 {number_count + 8}//5 {number_count + 4}//5
f {number_count + 5}//6 {number_count + 1}//6 {number_count + 4}//6 {number_count + 8}//6
"""
        obj_str += f"""v {self.full_size / 10} {self.full_size / 10} {0}
v {self.full_size / 10} {self.full_size / 10} {self.height_step}
v {-self.block_hw} {self.full_size / 10} {self.height_step}
v {-self.block_hw} {self.full_size / 10} {0}
v {self.full_size / 10} {self.block_hw + self.full_size / 10} {0}
v {self.full_size / 10} {self.block_hw + self.full_size / 10} {self.height_step}
v {-self.block_hw} {self.block_hw + self.full_size / 10} {self.height_step}
v {-self.block_hw} {self.block_hw + self.full_size / 10} {0}
"""
        number_count += 8
        flat_str += f"""f {number_count + 1}//1 {number_count + 2}//1 {number_count + 3}//1 {number_count + 4}//1
f {number_count + 5}//2 {number_count + 8}//2 {number_count + 7}//2 {number_count + 6}//2
f {number_count + 1}//3 {number_count + 5}//3 {number_count + 6}//3 {number_count + 2}//3
f {number_count + 2}//4 {number_count + 6}//4 {number_count + 7}//4 {number_count + 3}//4
f {number_count + 3}//5 {number_count + 7}//5 {number_count + 8}//5 {number_count + 4}//5
f {number_count + 5}//6 {number_count + 1}//6 {number_count + 4}//6 {number_count + 8}//6
"""
        obj_str += f"""v {0} {-self.block_hw} {0}
v {0} {-self.block_hw} {self.height_step}
v {-self.block_hw} {-self.block_hw} {self.height_step}
v {-self.block_hw} {-self.block_hw} {0}
v {0} {self.full_size / 10} {0}
v {0} {self.full_size / 10} {self.height_step}
v {-self.block_hw} {self.full_size / 10} {self.height_step}
v {-self.block_hw} {self.full_size / 10} {0}
"""
        number_count += 8
        flat_str += f"""f {number_count + 1}//1 {number_count + 2}//1 {number_count + 3}//1 {number_count + 4}//1
f {number_count + 5}//2 {number_count + 8}//2 {number_count + 7}//2 {number_count + 6}//2
f {number_count + 1}//3 {number_count + 5}//3 {number_count + 6}//3 {number_count + 2}//3
f {number_count + 2}//4 {number_count + 6}//4 {number_count + 7}//4 {number_count + 3}//4
f {number_count + 3}//5 {number_count + 7}//5 {number_count + 8}//5 {number_count + 4}//5
f {number_count + 5}//6 {number_count + 1}//6 {number_count + 4}//6 {number_count + 8}//6
"""
        return obj_str, flat_str
